from __future__ import unicode_literals
import frappe
import json
from frappe.utils import nowdate, today
from profiles.provider_api import update_online_status
from fire_base_admin.rdb import change_order_status, update_provider
from fire_base_admin.fire_store import get_real_providers

from notifications.api import create_notifications
from frappe.utils.background_jobs import enqueue

def update_order_count(status, order):

    if status == 'Work Completed':
        customer_count = frappe.get_value('Customer', order.customer, 'total_order')
        provider_count = frappe.get_value('Provider', order.provider, 'total_order')
        frappe.db.set_value('Customer', order.customer, 'total_order', customer_count + 1)
        frappe.db.set_value('Provider', order.provider, 'total_order', provider_count + 1)


@frappe.whitelist()
def change_status(**kargs):
    statauses = ['Accepted', 'In Transit', 'Destination Reached',
                    'Work Completed', 'Cancelled']

    status = kargs['status']

    order = frappe.get_doc('Service Order', kargs['order'])

    if status in statauses:
        if status == 'Accepted':
            order.provider = frappe.session.user
        order.status = status
        order.flags.ignore_permissions = True
        order.save()


@frappe.whitelist()
def process_status(order, method):
    status = order.status
    order_table = 'Service Order'

    statauses = ['Accepted', 'In Transit', 'Destination Reached',
                    'Work Completed', 'Cancelled']

    if status in statauses:
        
        if status == 'Cancelled' or status == 'Work Completed':
            msg = 'Provider cancelled your order' if status == 'Cancelled' else 'Destination Reached'
            
            invoice = frappe.get_doc({
                'doctype': 'Service Invoice',
                'customer': order.customer,
                'provider': order.provider,
                'total_qty': order.total_qty,
                'total': order.total,
                'net_total': order.net_total,
                'total_taxes_and_charges': order.total_taxes_and_charges,
                'discount_amount': order.discount_amount,
                'grand_total': order.grand_total,
                'rounding_adjustment': order.rounding_adjustment,
                'start_address': order.start_address,
                'des_address': order.des_address,
                'vendor': order.vendor,
                'driver_pay': 0 if status == 'Cancelled' else order.driver_pay,
                'commission_rate': 0 if status == 'Cancelled' else order.commission_rate,
                'total_commission': 0 if status == 'Cancelled' else order.total_commission,
                'offer': order.offer,
                'payment_method': order.payment_method,
                'amended_from': order.name,
                'status': 'Cancelled' if status == 'Cancelled' else 'Closed',
                'payment_status': order.payment_status,
                'start_lat': order.start_lat,
                'start_lon': order.start_lon,
                'end_lat': order.end_lat,
                'end_lon': order.end_lon,
                'ride_date': order.ride_date,
                'ride_type': order.ride_type,
                'vh_re': order.vh_re,
            })
            invoice.flags.ignore_permissions = True
            invoice.insert()
            order.flags.ignore_permissions = True
            order.delete()
            if invoice.provider:
                update_online_status(is_customer=True, is_online=1, provider=invoice.provider)
            order = invoice
            order_table = 'Service Invoice'
            customer_user = invoice.customer
        else:
            if status == 'Accepted':
                msg = 'Provider accepted your order'
            elif status == 'In Transit':
                msg = 'Provider is in transit'
            elif status == 'Destination Reached':
                msg = 'Your Vehicle has been picked up.'
            elif status == 'Work Started':
                msg = 'Service Started'

            # frappe.db.set_value('Service Order', order.name, 'status', status)
            customer_user = order.customer
        # change_order_status(order.name, status)
        notification = frappe.get_doc({
            'doctype': 'App Notifications',
            'title': status,
            'sender': frappe.session.user,
            'recipient': customer_user,
            'message': msg,
            'order_table': order_table,
            'order_name': order.name,
        })
        create_notifications(notification)
        update_order_count(status, order)
    return True


def handle_cancel(order):
    cancelled_by = [order.provider]
    can_spil = list(filter(None, order.cancelled_by.split(",") if order.cancelled_by else list()))
    cancelled_by = can_spil + cancelled_by

    order.provider = ''
    order.status = "Left-of"

    order_data = order.__dict__

    if len(cancelled_by) <= 5:
        providers = get_real_providers(order_data)
        if len(providers) > 0:
            for pro in providers:
                if pro['id'] not in cancelled_by:
                    order.provider = pro['id']
                    order.status = "Pending"
                    break
                    
    order.cancelled_by = ",".join(cancelled_by)
    order.save()

@frappe.whitelist()
def cancel_order(**kargs):
    order = frappe.get_doc('Service Order', kargs['order'])
    update_provider(order.name, '')
    enqueue(handle_cancel, 'short', event='all', order=order)
    


@frappe.whitelist()
def list_provider_active_orders(limit_start=0):
    doctype = 'Service Order'
    filters = [['provider', '=', frappe.session.user],]
    data = frappe.get_list(doctype, fields=[
        'name',
        'customer',
        'provider',
        'creation',
        'start_lat',
        'start_lon',
        'end_lat',
        'end_lon',
        'payment_status',
        'status'],
        filters=filters,
        order_by='creation desc',
        limit_start=limit_start)
    return data


@frappe.whitelist()
def list_provider_previous_orders(limit_start=0):
    doctype = 'Service Invoice'
    filters = [['provider', '=', frappe.session.user], ['status', '=', 'Closed']]
    data = frappe.get_list(doctype, fields=[
        'name',
        'customer',
        'provider',
        'creation',
        'start_lat',
        'start_lon',
        'end_lat',
        'end_lon',
        'ride_type',
        'payment_status',
        'grand_total'],
        filters=filters,
        order_by='creation desc',
        limit_start=limit_start)
    return data


@frappe.whitelist()
def total_earnings():
    doctype = 'Service Invoice'
    res = frappe.db.sql("select SUM(`grand_total`) as total_eranings from `tabService Invoice` where provider=%s and status='Closed'",
                        (frappe.session.user,), as_dict=True)
    return res
