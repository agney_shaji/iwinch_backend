import frappe
import json
import requests 
from fire_base_admin.rdb import change_payment_status


@frappe.whitelist(allow_guest=True,)
def validate_payment(**kargs):
    # url = "https://www.paytabs.com/apiv2/verify_payment"
    # data = {
    #     'merchant_email': 'info@iwinch.com',
    #     'secret_key': 'CWc3wNaBKvKITEWQH9X06IgeQj5kjvCWrPZYCaznFk6zZ3pGcmGj4WxVgCfz8SvzHvykF7IlsOUrRj83FPmKMUG2xVBFO7fvCNQh',
    #     'payment_reference': kargs['payment_reference']
    # }
    # r = requests.post(url = url, data = data) 
    # responce = r.json()
    # if responce['response_code'] == "100":
    order = frappe.get_doc('Service Order', kargs['order_id'])
    order.payment_status = "Fully Paid"
    doctype = 'Payment Details'
    payment_res = frappe.new_doc(doctype)
    payment_res.update({
        'parentfield': 'payment',
        'transaction_id': kargs.get('transaction_id', 'sdsvsdsdvsdvsdvsd'),
        'payment_reference': kargs.get('pt_invoice_id', '235gf3t346'),
        'amount_due': 0,
        'amount_paid': order.grand_total,
        'card_brand': kargs.get('card_brand', 'sss'),
        'card_first_six_digits': kargs.get('card_first_six_digits', '525732'),
        'card_last_four_digits': kargs.get('card_last_four_digits', '789654'),
        'response_code': kargs.get('response_code', '200'),
    })
    order.payment.append(payment_res)
    order.flags.ignore_permissions = True
    order.save(ignore_version=True)
    change_payment_status(order)
    return_data = {'status': True, 'data': kargs}
    return return_data
    
@frappe.whitelist()
def create_cash_payment(**kargs):

    order = frappe.get_doc('Service Order', kargs['order'])
    doctype = 'Payment Details'
    payment_res = frappe.new_doc(doctype)
    payment_res.update({
        'parentfield': 'payment',
        'transaction_id': 'cash_pay',
        'card_brand': 'cash',
        'amount_due': 0,
        'amount_paid': order.grand_total,
    })
    order.payment_status = "Fully Paid"
    order.payment.append(payment_res)
    order.flags.ignore_permissions = True
    order.save(ignore_version=True)
    change_payment_status(order)

    return True