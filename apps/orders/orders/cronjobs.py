from __future__ import unicode_literals
import frappe
from frappe.utils import now, add_to_date
from fire_base_admin.fire_store import get_real_providers
from fire_base_admin.rdb import update_provider
from frappe.utils.background_jobs import enqueue

from orders.api_provider import change_status


def match(order):
    providers = get_real_providers(order, first=True)
    if len(providers) > 0:
        update_provider(order['name'], providers[0]['id'])
        frappe.db.set_value("Service Order", order['name'], "provider", providers[0]['id'])
    else:
        status = "Left-of"
        frappe.db.set_value("Service Order", order['name'], {"status":status, "provider": ""})


@frappe.whitelist()
def allocate_shedule():
    # least_time = frappe.db.get_single_value("Last Run", "on")
    current_time = now()
    added_time = add_to_date(current_time, minutes=15)
    exp_time = add_to_date(current_time, minutes=-3)

    allo_filters = {
        # 'ride_date': ['>', least_time],
        'ride_date': ['<=', added_time],
        'status': ['in', ["Pending", "Left-of"]],
        'ride_type': 'scheduled',
        }
    
    orders = frappe.get_list('Service Order',
                fields=[
                    'name',
                    'vh_type',
                    'service',
                    'ride_date',
                    'start_lat',
                    'start_lon',],
                filters=allo_filters,
                order_by="ride_date")
    print(orders)
    for order in orders:
        enqueue(match, 'short', event='all', order=order)
    
    exp_filters = {
        # 'ride_date': ['>', least_time],
        'ride_date': ['<=', exp_time],
        'status': "Left-of",
        'ride_type': 'scheduled',
        }
    
    exp_orders = frappe.get_list('Service Order',
                fields=[
                    'name',
                    'vh_re',
                    'ride_date',
                    'start_lat',
                    'start_lon',],
                filters=exp_filters,
                order_by="ride_date")

    for order in exp_orders:
        enqueue(change_status, 'short', event='all', order=order, status='Cancelled')

    # frappe.db.set_value("Last Run", "on", current_time)
        


