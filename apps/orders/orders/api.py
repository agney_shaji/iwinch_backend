from __future__ import unicode_literals
import frappe
import json
from frappe.utils import now, today
from discounts.api import offer_detail
from profiles.api import get_logged_customer, get_logged_provider, get_provider_detail
from fire_base_admin.rdb import order_watch
from orders.orders.doctype.service_order.service_order import calulate_service_total

@frappe.whitelist()
def create_order(**kargs):
    doctype = 'Service Order'
    now_date = now()

    pay_method = kargs.get('payment_method', 'Cash')
    offer = kargs.get('offer', '')
    ride_date = kargs.get('ride_date', now_date)
    ride_date = ride_date if ride_date != '' else now_date
    ride_type = 'immediate' if ride_date == now_date else 'scheduled'

    order = frappe.get_doc({
        'doctype': doctype,
        'customer': frappe.session.user,
        'total_qty':  kargs['total_qty'],
        'offer':  offer if offer != 'null' else '',
        'payment_method': pay_method,
        'status': 'Pending',
        'start_address': kargs.get('start_address'),
        'des_address': kargs.get('des_address'),
        'service': kargs.get('service'),
        'region': kargs.get('admin_area'),
        'vh_type': kargs.get('vh_type'),
        'start_lat': kargs['start_lat'],
        'start_lon': kargs['start_lon'],
        'end_lat': kargs['end_lat'],
        'end_lon': kargs['end_lon'],
        'ride_date': ride_date,
        'ride_type': ride_type,
    })

    order.run_method('validate')
    order = calulate_service_total(order)
    if pay_method != "Cash":
        doctype = 'Payment Details'
        payment_res = frappe.new_doc(doctype)
        payment_res.update({
            'parentfield': 'payment',
            'transaction_id': 'card_init',
            'amount_due': order.grand_total,
            'amount_paid': 0,
        })
    else:
        doctype = 'Payment Details'
        payment_res = frappe.new_doc(doctype)
        payment_res.update({
            'parentfield': 'payment',
            'transaction_id': 'cash_init',
            'amount_due': order.grand_total,
            'amount_paid': 0,
        })

    order.flags.ignore_permissions = True
    order.payment = [payment_res]
    order.insert()
    
    return order

@frappe.whitelist()
def create_order_provider(**kargs):
    frappe.db.set_value(
            'Service Order', kargs['order'], 'provider', kargs['provider'])


@frappe.whitelist()
def validate_offer(**kargs):
    offer_name = frappe.db.get_value("Offers", filters={"coupon_code": kargs['coupon_code']}, fieldname="name")
    offer = frappe.get_doc('Offers', offer_name)
    return offer

@frappe.whitelist()
def order_detail(order_name):
    try:
        order = frappe.get_doc('Service Invoice', order_name)
    except:
        order = frappe.get_doc('Service Order', order_name)
    return order

@frappe.whitelist()
def allocateorder(order_name):
    order = frappe.get_doc('Service Order', order_name)
    order_watch(order)
