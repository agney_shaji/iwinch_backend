# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"module_name": "Orders",
			"color": "yellow",
			"icon": "fa fa-cart-plus",
			"type": "module",
			"label": _("Sales")
		}
	]
