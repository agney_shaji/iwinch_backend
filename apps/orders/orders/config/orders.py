from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"label": _("Sales"),
			"icon": "fa fa-star",
			"items": [
				{
					"type": "doctype",
					"name": "Service Order",
					"label": _("Ongoing Order List"),
					"description": _("Orders yet to completed"),
				},
				{
					"type": "doctype",
					"name": "Service Invoice",
					"label": _("Completed Order List"),
					"description": _("Oredrs converted to Invoices"),
				},
			],
		},
	]
