# -*- coding: utf-8 -*-
# Copyright (c) 2019, Agney Shaji and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import math
# import frappe
from frappe.model.document import Document
from frappe.utils import flt, cint, cstr, today, getdate
import frappe
# from fire_base_admin.fire_store import create_order, delete_order
from fire_base_admin.fire_store import update_status
from fire_base_admin.rdb import create_order, delete_order, update_order


class ServiceOrder(Document):
	def validate(self):
		self.new_flag = self.is_new()
		if not self.name or self.is_new():
			self.validate_services()
		if self.offer and (self.is_new() or not self.name):
			self.validate_offer()
		self.validate_provider()

	def validate_offer(self):
		offer = frappe.get_doc('Offers', self.offer)
		status = True
		msg = "Offer successfully applied"

		if offer.is_active:
			if not offer.region or offer.region == self.region:
				if offer.start_date <= getdate(today()) <= offer.end_date:
					if offer.max_usage > 0:
						order_count = frappe.db.count('Service Order', {'offer': self.offer})

						usage_count = frappe.db.count('Offer Usage', {'offer': self.offer})

						total_offer_count = order_count + usage_count

						if offer.max_usage <= total_offer_count:
							status = False
							msg = "Maximum user limit exceeded"
							self.offer = ""

					if offer.max_user > 0:
						order_user_count = frappe.db.count('Service Order', {'customer': self.customer, 'offer': self.offer})
					
						user_count = frappe.get_value('Offer Usage', {'customer': self.customer, 'offer': self.offer}, ['appl_count'])

						user_offer_count = order_user_count + user_count

						if offer.max_user <= user_offer_count:
							status = False
							msg = "Maximum usage limit exceeded"
							self.offer = ""
			else:
				status = False
				msg = "Offer not avilable in this region"
		else:
			status = False
			msg = "Offer is invalid"
			self.offer = ""
		frappe.response["offer_applied"] = status
		frappe.response["offer_msg"] = msg
		frappe.response["offer"] = offer

	def validate_services(self):
		if self.region:
			region = self.region.upper()
			vh_type = self.vh_type
			vh_service = self.service
			name = "{}-{}-{}".format(region, vh_type, vh_service)
			service = frappe.get_doc('Region Pricing', name)
			self.vh_re = service.name
		else:
			frappe.response['msg'] = 'servive not avilable'
			frappe.throw("servive no avilable")

	def validate_provider(self):
		vendor = frappe.db.get_value('Provider', self.provider, 'vendor')
		self.vendor = vendor

	def before_save(self):
		self.change_status()
		if self.is_new():
			self = calulate_service_total(self)
	
	def change_status(self):
		if self.status == 'Left-of' and self.provider:
			self.status = 'Pending'

	def after_insert(self):
		create_order(self)

	def on_update(self):
		if self.provider and self.status == "Pending":
			update_status(self.provider, "engage")
		if not self.new_flag:
			update_order(self)

	def on_trash(self):
		delete_order(self.name)


def calulate_service_total(order):
	service = frappe.get_doc('Region Pricing', order.vh_re)
	qty = flt(order.total_qty)
	if qty <= service.base_km:
		qty = 0
	else:
		total = flt(service.base_price)
		qty -= service.base_km
	total = flt(service.base_price)
	extra = qty * service.km_price
	total += extra
	order.total = total
	order.commission_rate = service.com_per
	order.total_commission =  total*(flt(service.com_per)/flt(100))
	order.driver_pay = total - order.total_commission
	discount = 0.0
	if order.offer:
		offer = frappe.get_doc('Offers', order.offer)
		discount = total*(flt(offer.disc_per)/flt(100))
		if offer.max_disc_amt:
			discount = discount if discount < offer.max_disc_amt else offer.max_disc_amt
		total = total - discount
	order.discount_amount = discount
	order.net_total = total
	frac, whole = math.modf(total)
	rounding_adjustment = 0.0
	if frac != 0.0:
		if frac < 0.5:
			rounding_adjustment = 0 - frac
			total = whole
		else:
			rounding_adjustment = 0 + frac
			total = whole + 1
	order.rounding_adjustment = rounding_adjustment
	order.grand_total = total
		
	return order

