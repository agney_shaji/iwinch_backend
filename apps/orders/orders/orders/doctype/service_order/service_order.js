// Copyright (c) 2019, Agney Shaji and contributors
// For license information, please see license.txt

frappe.ui.form.on('Service Order', {
	refresh: function(frm) {

        var path = "origin="+frm.doc.start_lat+","+frm.doc.start_lon+"&destination="+frm.doc.end_lat+","+frm.doc.end_lon
		var map_div = document.getElementById("heatmap-service_order")
		console.log(path)
		map_div.innerHTML = ''
		var iframe = $('<iframe />');
		iframe.prop('width', '700')
		iframe.prop('height', '300')
		iframe.prop('frameborder', '0')
		iframe.prop('style', 'border:0')
		iframe.prop('src', 'https://www.google.com/maps/embed/v1/directions?'+path+'&key=AIzaSyBFgzqVuzbH67_k8ehMiA08e7BJaBHdz40');
		// map_div.appendChild(iframe); 
		iframe.appendTo($('#heatmap-service_order'));
	}
});
