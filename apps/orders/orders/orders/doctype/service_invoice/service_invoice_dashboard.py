from __future__ import unicode_literals
from frappe import _

def get_data():
	return {
		# 'graph': True,
		# 'graph_method': "profiles.api_admin.test_chart",
		# 'graph_method_args': {
		# 	'chart_name': 'Total Income',
		# 	'filters': '{"status":"Closed"}',
		# 	'refresh': '1'
		# },
		'heatmap': True,
		'heatmap_message': _('This is based order'),
		'fieldname': 'order',
		'transactions': [
			{
				# 'label': _('Feedbacks'),
				'items': [ 'Provider Feedback', ]
			},
			{
				# 'label': _('Sales'),
				'items': ['Customer Feedback',	]
			},
		# 	{
		# 		'label': _('Purchase'),
		# 		'items': ['Purchase Order', 'Purchase Receipt', 'Purchase Invoice']
		# 	},
		]
	}
