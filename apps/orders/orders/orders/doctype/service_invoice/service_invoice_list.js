// frappe.listview_settings['Service Invoice'] = {
// 	add_fields: ["customer_name", "base_grand_total", "status",
// 		"company", "currency", 'valid_till'],

// 	onload: function(listview) {
// 		listview.page.fields_dict.provider.get_query = function() {
// 			return {
// 				"filters": {
// 					"name": ["in", ["Customer", "Lead"]],
// 				}
// 			};
// 		};
// 	},
// };

frappe.listview_settings['Service Invoice'] = {
	hide_name_column: true,
	filters: [["status","=","Closed"]],

	// onload: function(me) {
	// 	if (!frappe.route_options) {
	// 		frappe.route_options = {
	// 			"payment_method": "Cash"
	// 		};
	// 	}
	// 	// me.page.set_title(__("To Do"));
	// 	// $('.list-row-activity').each(function (e) {
	// 	// 	e.innerHTML = ''
	// 	// })
	// },
	

	// button: {
	// 	show: function(doc) {
	// 		return doc.status;
	// 	},
	// 	get_label: function() {
	// 		return __('Open');
	// 	},
	// 	get_description: function(doc) {
	// 		return __('Open {0}', [`${doc.reference_type} ${doc.reference_name}`])
	// 	},
	// 	action: function(doc) {
	// 		frappe.set_route('Form', doc.reference_type, doc.reference_name);
	// 	}
	// },


}