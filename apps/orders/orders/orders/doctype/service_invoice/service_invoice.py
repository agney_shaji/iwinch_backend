# -*- coding: utf-8 -*-
# Copyright (c) 2019, Agney Shaji and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
# import frappe
from frappe.model.document import Document

class ServiceInvoice(Document):

	def before_save(self):
		self.name = self.amended_from
		if self.status == 'Cancelled' and self.payment_status == 'Fully Paid':
			self.payment_status = 'Refund-Pending'
		if self.status == 'Closed':
			self.total_commission = self.grand_total - self.driver_pay
		

		


