# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"module_name": "Earnings",
			"color": "grey",
			"icon": "fa fa-money",
			"type": "module",
			"label": _("Earnings Requests")
		}
	]
