from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"label": _("Earnings"),
			"icon": "fa fa-star",
			"items": [
				{
					"type": "doctype",
					"name": "Payment Request",
					"label": _("Drivers Request list"),
					"description": _("Drivers Payment Request list"),
				},
				{
					"type": "doctype",
					"name": "Payment Profile",
					"label": _("Drivers Payment Profile"),
					"description": _("Drivers Payment Profile"),
				},
			],
		},
	]
