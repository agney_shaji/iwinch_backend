# -*- coding: utf-8 -*-
# Copyright (c) 2020, agney and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class PaymentProfile(Document):
	pass



def get_timeline_data(doctype, name):
	'''Return timeline for requests'''
	print('------------------------------------------')
	print(name)
	a = dict(frappe.db.sql('''select unix_timestamp(`reqs_on`), SUM(`amt`)
		from `tabPayment Request` where
			reqs_by=%s
			and `reqs_on` > date_sub(curdate(), interval 1 year)
			and is_approved = 1
			group by reqs_on''', name)) 
	print(a)
	return a