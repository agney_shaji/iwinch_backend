from __future__ import unicode_literals
from frappe import _

def get_data():
	return {
		# 'graph': True,
		# 'graph_method': "profiles.api_admin.test_chart",
		# 'graph_method_args': {
		# 	'chart_name': 'Total Income',
		# 	'filters': '{"status":"Closed"}',
		# 	'refresh': '1'
		# },
		'heatmap': True,
		'heatmap_message': _('This is based on the approved Payment Requests of this Provider'),
		'fieldname': 'reqs_by',
		'transactions': [
			{
				# 'label': _('Feedbacks'),
				'items': [ 'Payment Request', ]
			},
		]
	}
