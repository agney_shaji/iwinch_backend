from __future__ import unicode_literals
import frappe

@frappe.whitelist()
def create_card(**kargs):
    # print(frappe.form_dict)
    doctype = 'Cards'
    card = frappe.get_doc({
        'doctype': doctype,
        'card_number': kargs['number'],
        'card_cvv': kargs['cvv'],
        'card_name': kargs['name'],
        'exp_month': kargs['month'],
        'expiry_year': kargs['year'],
        'customer': frappe.session.user,
    })
    # card.flags.ignore_permissions = True
    card.insert()
    return card


@frappe.whitelist()
def list_cards():
    # print(frappe.form_dict)
    doctype = 'Cards'
    filters = [['customer', '=', '{}'.format(frappe.session.user)],]

    data = frappe.get_list(doctype, fields=[
        'name',
        'card_number',
        'card_name',
        'exp_month',
        'expiry_year',],
        filters=filters,)

    return data