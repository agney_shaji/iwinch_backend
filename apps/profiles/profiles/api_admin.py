from __future__ import unicode_literals
import frappe
from profiles.helper import buid_filters
from profiles.api import get_customer_detail
from frappe.desk.doctype.dashboard_chart.dashboard_chart import get


@frappe.whitelist()
def list_riders(filters='{"name": ""}', start=0, order_by='creation desc', page_length=10):

    doctype = 'Customer'

    filter_dict = buid_filters(filters)

    data = frappe.get_list(doctype, fields=[
        'name',
        'full_name',
        'mobile_no',
        'email_id',
        'DATE_FORMAT(creation, "%e-%m-%Y %H:%i") as creation',
        'rating',],
        filters=filter_dict,
        order_by=order_by,
        page_length=page_length,
        start=start)

    count = frappe.get_all(doctype, fields=["COUNT(*) as count"], filters=filter_dict)[0]['count']
    result = {'users': data, 'count': count}
    return result


@frappe.whitelist()
def list_drivers(filters='{"name": ""}', start=0, order_by='creation ', page_length=10):
    doctype = 'Provider'
    filter_dict = buid_filters(filters)
    # filter_dict['disabled'] = 0
    filter_dict['new_reg'] = 0
    data = frappe.get_list(doctype, fields=[
        'name',
        'full_name',
        'mobile_no',
        'email_id',
        'vehicle_number',
        'license_no',
        'DATE_FORMAT(creation, "%e-%m-%Y %H:%i") as creation',
        'rating',],
        filters=filter_dict,
        order_by=order_by,
        page_length=page_length,
        start=start)
    count = frappe.get_all(doctype, fields=["COUNT(*) as count"], filters=filter_dict)[0]['count']

    return {'drivers': data, 'count': count}


@frappe.whitelist()
def list_approvals(filters='{"name": ""}', start=0, order_by='creation desc', page_length=10):
    doctype = 'Provider'
    filter_dict = buid_filters(filters)
    filter_dict['disabled'] = 1
    filter_dict['new_reg'] = 1
    
    data = frappe.get_list(doctype, fields=[
        'name',
        'full_name',
        'mobile_no',
        'email_id',
        'vehicle_number',
        'license_no',
        'DATE_FORMAT(creation, "%e-%m-%Y %H:%i") as creation',
        'rating',],
        filters=filter_dict,
        order_by=order_by,
        page_length=page_length,
        start=start)
    count = frappe.get_all(doctype, fields=["COUNT(*) as count"], filters=filter_dict)[0]['count']

    return {'drivers': data, 'count': count}


@frappe.whitelist()
def get_user(name):
    customer = frappe.get_doc('Customer', name)
    return customer

@frappe.whitelist()
def get_driver(name):
    provider = frappe.get_doc('Provider', name)
    # try:
    #     vh_type = frappe.get_doc('Vehicle Type', provider.vh_type)
    #     provider.vh_name = vh_type.title
    # except:
    #     pass
    return provider

@frappe.whitelist()
def update_customer_name(**kargs):
    user_details = frappe.get_doc("User", kargs['name'])
    user_details.first_name = kargs['first_name']
    user_details.last_name = kargs['second_name']
    user_details.flags.ignore_permissions = True
    user_details.save()
    customer = get_customer_detail(kargs['name'])
    customer.first_name = kargs['first_name']
    customer.second_name = kargs['second_name']
    customer.full_name = customer.first_name + " " +customer.second_name
    customer.email_id = kargs['email_id']
    customer.image = kargs['image']
    customer.flags.ignore_permissions = True
    customer.save()
    return customer

@frappe.whitelist()
def update_provider_name(**kargs):
    user_details = frappe.get_doc("User", kargs['name'])
    user_details.first_name = kargs['first_name']
    user_details.last_name = kargs['second_name']
    user_details.flags.ignore_permissions = True
    user_details.save()
    privider = get_driver(kargs['name'])
    privider.first_name = kargs['first_name']
    privider.second_name = kargs['second_name']
    privider.full_name = privider.first_name + " " + privider.second_name
    privider.email_id = kargs['email_id']
    privider.disabled = 1 if kargs['disabled'] == "true" else 0
    privider.license_no = kargs['license_no']
    privider.acc_no = kargs['acc_no']
    privider.em_id = kargs['em_id']
    privider.visa_no = kargs['visa_no']
    privider.flags.ignore_permissions = True
    privider.save()
    
    return privider

@frappe.whitelist()
def approve_driver(name, status):
    provider = frappe.get_doc('Provider', name)
    if status == "1":
        provider.disabled = 0
        provider.save()
    else:
        provider.delete()
    return True


@frappe.whitelist()
def provider_chart(name):

    pro_orders = dict(frappe.db.sql('''select DATE(`creation`) AS `Date`, COUNT(*)
		                            from `tabService Invoice` where
			                        provider=%s
			                        and MONTH(creation) = MONTH(NOW())
                                    and YEAR(creation) = YEAR(NOW())
			                        and status = 'Closed'
			                        group by Date''', name))

    avg_orders = dict(frappe.db.sql('''select DATE(`creation`) AS `Date`, IFNULL(COUNT(*) / COUNT(DISTINCT `provider`), 0) AS avrageorder
                                    from `tabService Invoice` where
			                        MONTH(creation) = MONTH(NOW())
                                    and YEAR(creation) = YEAR(NOW())
			                        and status = 'Closed'
			                        group by Date'''))
    label = list()
    pro_values = list()
    avg_values = list()
    for key, value in avg_orders.items():
        label.append(key)
        avg_values.append(value)
        pro_values.append(pro_orders.get(key, 0))
    datasets = {'label': label, 'pro_values': pro_values, 'avg_values': avg_values}

    return datasets


@frappe.whitelist()
def customer_chart(name):

    cus_orders = dict(frappe.db.sql('''select MONTHNAME(`creation`) AS `month`, COUNT(*)
		                            from `tabService Invoice` where
			                        customer=%s
                                    and YEAR(creation) = YEAR(NOW())
			                        and status = 'Closed'
			                        group by MONTH(`creation`)''', name)) 
    label = list()
    cus_values = list()
    for key, value in cus_orders.items():
        label.append(key)
        cus_values.append(value)
    datasets = {'label': label, 'cus_values': cus_values,}

    return datasets