from __future__ import unicode_literals
import frappe
from frappe.utils.background_jobs import enqueue
from fire_base_admin.rdb import download_file
from firebase_admin import auth


def create_file_entry(file_name, file_url):
    file_doc = frappe.new_doc("File")
    file_doc.file_name = file_name
    file_doc.attached_to_name = self.name
    file_doc.attached_to_doctype = self.doctype
    file_doc.file_url =  "/files/{0}".format(file_url)
    file_doc.save()

def download(provider):
    public_files = frappe.get_site_path('public', 'files')
    vhn_image_path = provider.name + "/" + "vehicle"
    license_image_path = provider.name + "/" + "license"
    em_id_image_path = provider.name + "/" + "emirates"
    visa_no_image_path = provider.name + "/" + "visa"

    if download_file(vhn_image_path, public_files, provider.name):
        provider.att_vhn_image = "/files/{0}".format(vhn_image_path)
        create_file_entry("vehicle", vhn_image_path)

    if download_file(license_image_path, public_files, provider.name):
        provider.att_license_image = "/files/{0}".format(license_image_path)
        create_file_entry("license", license_image_path)

    if download_file(em_id_image_path, public_files, provider.name):
        provider.att_em_id_image = "/files/{0}".format(em_id_image_path)
        create_file_entry("emirates", em_id_image_path)

    if download_file(visa_no_image_path, public_files, provider.name):
        provider.att_visa_no_image = "/files/{0}".format(visa_no_image_path)
        create_file_entry("visa", visa_no_image_path)

    provider.save()


def create_images(provider, method):

    # enqueue(create_notification, 'default', event='all', notification=notification)

    enqueue(download, 'short', event='all', provider=provider)


def create_user(provider, method):
   
    if provider.is_new():
        user_details = auth.create_user(phone_number=provider.mobile_no)
        print('Sucessfully created new user: {0}'.format(user_details.uid))
        uuid = user_details.uid
        user = frappe.get_doc({
            'doctype': 'User',
            'first_name': provider.first_name,
            'last_name': provider.second_name,
            'email': '{}@{}.uae'.format(uuid, "iwinch"),
            'uuid': uuid,
            'is_driver': 1,
        })
        user.flags.no_welcome_mail = True
        user.flags.ignore_permissions = True
        user.add_roles('Sales User')
        user = user.save()
        provider.account_manager = user.name
    
    return provider


def delete_user(provider):
    user = frappe.get_doc('User', provider.account_manager)
    try:
        a = auth.delete_user(user.uuid)
    except:
        pass
    frappe.db.delete('User', {'name': provider.account_manager})

    # return provider