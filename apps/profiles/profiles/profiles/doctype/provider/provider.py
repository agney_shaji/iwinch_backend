# -*- coding: utf-8 -*-
# Copyright (c) 2019, Agney and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
# import frappe
from frappe.model.document import Document
import frappe
from frappe import _
from fire_base_admin.fire_store import create_provider, delete_provider
from profiles.tasks import delete_user
import re

class Provider(Document):

	# pass
	def validate(self):
		self.flags.is_new_doc = self.is_new()

	def before_save(self):
		if self.flags.is_new_doc:
			frappe.db.set_value('User', self.account_manager, 'mobile_no', self.mobile_no)
			self.new_reg = 1
		else:
			user = frappe.get_doc('User', self.account_manager)
			user.first_name = self.first_name
			user.last_name = self.second_name
			user.email = self.email_id if self.email_id else user.email
			user.flags.ignore_permissions = True
			user.save()

		self.full_name = self.first_name + ((' ' + self.second_name) if self.second_name else '')

		if self.disabled == 0 and self.new_reg == 1:
			self.new_reg = 0
		if self.disabled == 1:
			frappe.db.set_value('User', self.account_manager, 'enabled', 0)
		else:
			frappe.db.set_value('User', self.account_manager, 'enabled', 1)
	
	def on_change(self):
		create_provider(self)


	def on_trash(self):
		delete_provider(self)
		delete_user(self)
	
	# def after_insert(self):
	# 	if not frappe.db.exists('Payment Profile', self.name):
	# 		pay_profile = frappe.get_doc({
	# 						'doctype': 'Payment Profile',
	# 						'provider': self.name,
	# 						'l_re_date': self.creation,
	# 						})
	# 		pay_profile.flags.ignore_permissions = True
	# 		pay_profile.save()
