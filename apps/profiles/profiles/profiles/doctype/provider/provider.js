// Copyright (c) 2019, Agney and contributors
// For license information, please see license.txt

frappe.ui.form.on('Provider', {
    refresh: function(frm) {

		frappe.call({
            "method": "profiles.api_admin.provider_chart",
            args: {
                doctype: "Library Member",
                name: frm.doc.name
            },
            callback: function (data) {
				console.log(data)
				let chart = new frappe.Chart("[class=heatmap]", { // or DOM element
				data: {
				labels: data.message.label,
			
				datasets: [
					{
						name: "Provider", chartType: 'bar',
						values: data.message.pro_values
					},
					{
						name: "Average", chartType: 'line',
						values: data.message.avg_values
					}
				],
				axisOptions: {
					xIsSeries: true // default: false
				},
				},
			
				title: "Driver orders VS Average Orders",
				type: 'axis-mixed', // or 'bar', 'line', 'pie', 'percentage'
				height: 300,
				colors: ['purple', '#ffa3ef', 'light-blue'],
			
				tooltipOptions: {
					formatTooltipX: d => (d + '').toUpperCase(),
					formatTooltipY: d => d + ' orders',
				}
				});
                
            }
        })
		// frm.add_custom_button(__("View Payment Profile"), function() {
		// 	frappe.set_route("Form", "Payment Profile", frm.doc.name);
		// }, "fa fa-table");
		
	}
});
