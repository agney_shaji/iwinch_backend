from __future__ import unicode_literals
from frappe import _

def get_data():
	return {
		# 'graph': True,
		# 'graph_method': "profiles.api_admin.test_chart",
		# 'graph_method_args': {
		# 	'chart_name': 'Total Income',
		# 	'filters': '{"status":"Closed"}',
		# 	'refresh': '1'
		# },
		'heatmap': True,
		'heatmap_message': _('This is based orders of this Driver'),
		'fieldname': 'provider',
		'transactions': [
			{
				'label': _('Orders'),
				'items': ['Service Order', 'Service Invoice',]
			},
			{
				'label': _('Feedbacks'),
				'items': [ 'Provider Feedback', 'Customer Feedback']
			},
			# {
			# 	'label': _('Earnings'),
			# 	'items': ['Payment Request']
			# },
		# 	{
		# 		'label': _('Sales'),
		# 		'items': ['Sales Order', 'Delivery Note', 'Sales Invoice']
		# 	},
		# 	{
		# 		'label': _('Purchase'),
		# 		'items': ['Purchase Order', 'Purchase Receipt', 'Purchase Invoice']
		# 	},
		]
	}
