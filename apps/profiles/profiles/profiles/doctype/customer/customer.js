// Copyright (c) 2019, Agney and contributors
// For license information, please see license.txt

frappe.ui.form.on('Customer', {
	refresh: function(frm) {
		
		frappe.call({
            "method": "profiles.api_admin.customer_chart",
            args: {
                doctype: "Library Member",
                name: frm.doc.name
            },
            callback: function (data) {
				console.log(data)
				let chart = new frappe.Chart("[class=heatmap]", { // or DOM element
				data: {
				labels: data.message.label,
			
				datasets: [
					{
						name: "Total customer Orders", chartType: 'line',
						values: data.message.cus_values
					}
				],
				axisOptions: {
					xIsSeries: true // default: false
				},
				},
			
				title: "Total Customer orders Month Wise",
				type: 'percentage', // or 'bar', 'line', 'pie', 'percentage'
				height: 100,
			
				tooltipOptions: {
					formatTooltipX: d => (d + '').toUpperCase(),
					formatTooltipY: d => d + ' orders',
				}
				});
                
            }
        })
	}
});

	
