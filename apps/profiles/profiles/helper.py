import json

def buid_filters(filters):
    print(filters)
    filters = json.loads(filters)
    filter_dict = dict()

    for key in filters:
        if filters[key]:
            if key in ["customer", "provider", "payment_method", "ride_type", "status"]:
                filter_dict[key] = filters[key]
            elif key in ["creation", "ride_date",]:
                filter_dict[key] = ['between', [filters[key], filters[key]+" 23:59:59"]]
            else:
                filter_dict[key] = ['like', filters[key]]

    return filter_dict