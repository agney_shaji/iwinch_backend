from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"label": _("Profiles"),
			"icon": "fa fa-star",
			"items": [
				{
					"type": "doctype",
					"name": "Customer",
					"label": _("Customer List"),
					"description": _("Customers App Users"),
				},
				{
					"type": "doctype",
					"name": "Provider",
					"label": _("Provider List"),
					"description": _("Provider App Users"),
				},
			],
		},
		{
			"label": _("Feedback"),
			"icon": "fa fa-star",
			"items": [
				{
					"type": "doctype",
					"name": "Customer Feedback",
					"label": _("Customer Feedback"),
					"description": _("Customers feedback on order and provider"),
				},
				{
					"type": "doctype",
					"name": "Provider Feedback",
					"label": _("Provider Feedback"),
					"description": _("Provider feedback on customer"),
				},
			],
		},
		{
			"label": _("Car Models"),
			"icon": "fa fa-star",
			"items": [
				{
					"type": "doctype",
					"name": "Car Models",
					"label": _("Car list"),
					"description": _("Customers feedback on order and provider"),
				},
			],
		},
		{
			"label": _("Community"),
			"icon": "fa fa-star",
			"items": [
				{
					"type": "doctype",
					"name": "Customer",
					"label": _("Customer List"),
					"description": _("Customers App Users"),
				},
				{
					"type": "doctype",
					"name": "Provider",
					"label": _("Provider List"),
					"description": _("Provider App Users"),
				},
			],
		},
	]
