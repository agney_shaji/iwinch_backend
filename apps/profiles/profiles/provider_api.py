from __future__ import unicode_literals
import frappe
from frappe.utils import flt
from profiles.api import get_provider_detail
from otp_login.api import create_token
from fire_base_admin.auth_fire import validate_user
from fire_base_admin.fire_store import create_provider, delete_provider
import os
from base64 import b32encode

@frappe.whitelist(allow_guest=True)
def register_provider(**kargs):

    uid = validate_user(kargs['id_token'])
    user = frappe.db.get_value("User", filters={"uuid": uid}, fieldname="name")
    if not user:
        doctype = 'Provider'
        profile = frappe.get_doc({
            'doctype': doctype,
            'first_name': kargs.get('first_name', ''),
            'disabled': 1,
            'mobile_no': kargs.get('mobile_no', ''),
            'email_id': kargs.get('email_id', ''),
            'second_name': kargs.get('surname', ''),
            'vehicle_number': kargs.get('vehicle_number', ''),
            'license_no': kargs.get('license_no', ''),
            'license_image': kargs.get('license_image', ''),
            'vhn_image': kargs.get('vhn_image', ''),
            'acc_no': kargs.get('acc_no', ''),
            'em_id_image' : kargs.get('em_id_image',''),
            'em_id' :  kargs.get('em_id',''),
            'visa_no_image' :  kargs.get('visa_no_image',''),
            'visa_no' :  kargs.get('visa_no',''),
            'vh_type' : kargs.get('vh_type', ''),
            'image' :  kargs.get('image', ''),
        })
        profile.flags.ignore_permissions = True
        profile.save()
        status = True
    else:
        status = False
    
    token = create_token(user)
    token['status'] = status 
    return token


@frappe.whitelist()
def create_feedback(**kargs):
    doctype = 'Provider Feedback'

    provider_name, customer_name = frappe.db.get_value('Service Invoice',
                                             kargs['order'], ['provider', 'customer'])
    feedback = frappe.get_doc({
        'doctype': doctype,
        'rating': kargs['rating'],
        'order': kargs['order'],
        'customer': customer_name,
        'provider': provider_name,
        'description': kargs.get('description', None),
    })

    feedback.flags.ignore_permissions = True
    feedback.save()

    provider = get_provider_detail(provider_name)
    provider.total_feefback += 1
    provider.total_rating += flt(feedback.rating)
    provider.rating = provider.total_rating / provider.total_feefback

    frappe.db.set_value('Provider', provider_name, {
    'total_feefback': provider.total_feefback,
    'total_rating':  provider.total_rating,
    'rating': provider.rating,
    })
    return feedback


@frappe.whitelist()
def update_online_status(is_customer=False, **kargs):
    if not is_customer:
        provider = get_provider_detail(frappe.session.user)
    else:
        provider = get_provider_detail(kargs['provider'])
    frappe.db.set_value(
                'Provider', frappe.session.user, 'is_online', kargs['is_online'])
    if kargs['is_online'] == 1:
        create_provider(provider)
    else:
        delete_provider(provider)
    return True
