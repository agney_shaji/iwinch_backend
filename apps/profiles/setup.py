# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

with open('requirements.txt') as f:
	install_requires = f.read().strip().split('\n')

# get version from __version__ variable in profiles/__init__.py
from profiles import __version__ as version

setup(
	name='profiles',
	version=version,
	description='Diffrent user profiles',
	author='Agney',
	author_email='agney@deienami.com',
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)
