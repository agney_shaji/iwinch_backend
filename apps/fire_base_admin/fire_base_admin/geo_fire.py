from geoindex import GeoGridIndex, GeoPoint
import geohash
from geoindex import utils
from geoindex.geo_grid_index import GEO_HASH_GRID_SIZE
import requests

# GeoFire for Python - By Guanjiu Zhang
class GeoFire(GeoGridIndex):
    def __init__(self,
                 lat,
                 lon,
                 radius,
                 unit='km'
                 ):
        GeoGridIndex.__init__(self,precision=3)
        self.center_point = GeoPoint(
            latitude=float(lat),
            longitude=float(lon)
        )
        self.radius = radius
        self.unit = unit
        self._config = None


    # Get all nearest regions geohashes to form a search circle
    def __get_search_region_geohashes(self):
        if self.unit == 'mi':
            self.radius = utils.mi_to_km(self.radius)
        grid_size = GEO_HASH_GRID_SIZE[self.precision]
        if self.radius > grid_size / 2:
            # radius is too big for current grid, we cannot use 9 neighbors
            # to cover all possible points
            suggested_precision = 0
            for precision, max_size in GEO_HASH_GRID_SIZE.items():
                if self.radius > max_size / 2:
                    suggested_precision = precision - 1
                    break
            raise ValueError(
                'Too large radius, please rebuild GeoHashGrid with '
                'precision={0}'.format(suggested_precision)
            )
        search_region_geohashes = geohash.expand(self.get_point_hash(self.center_point))
        return search_region_geohashes

    def query_nearby_objects(self, query_ref, geohash_ref, token_id = None):
        search_region_hashes = self.__get_search_region_geohashes()
        for search_region_hash in search_region_hashes:
            print(search_region_hash)
            try:
                end_hash = search_region_hash + '\uf8ff'
                nearby_objects = query_ref.order_by(u''+geohash_ref).\
                    start_at({u''+geohash_ref: u'' + search_region_hash}).end_at({u''+geohash_ref: u'' + end_hash}).get()
                providers_set = list()
                for doc in nearby_objects:
                    pro_dict = doc.to_dict()
                    pro_dict["id"] = doc.id
                    providers_set.append(pro_dict)
                    all_nearby_objects.update(pro_dict)
            except requests.HTTPError as error:
                raise error
            except:
                continue
        # print(len(providers_set))
        return providers_set