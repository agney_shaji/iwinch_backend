# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from . import __version__ as app_version

app_name = "fire_base_admin"
app_title = "Fire Base Admin"
app_publisher = "agney@deienami.com"
app_description = "firebase admin operations"
app_icon = "octicon octicon-file-directory"
app_color = "grey"
app_email = "agney@deienami.com"
app_license = "MIT"

# Includes in <head>
# ------------------

# include js, css files in header of desk.html
# app_include_css = "/assets/fire_base_admin/css/fire_base_admin.css"
# app_include_js = "/assets/fire_base_admin/js/fire_base_admin.js"

# include js, css files in header of web template
# web_include_css = "/assets/fire_base_admin/css/fire_base_admin.css"
# web_include_js = "/assets/fire_base_admin/js/fire_base_admin.js"

# include js in page
# page_js = {"page" : "public/js/file.js"}

# include js in doctype views
# doctype_js = {"doctype" : "public/js/doctype.js"}
# doctype_list_js = {"doctype" : "public/js/doctype_list.js"}
# doctype_tree_js = {"doctype" : "public/js/doctype_tree.js"}
# doctype_calendar_js = {"doctype" : "public/js/doctype_calendar.js"}

# Home Pages
# ----------

# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
#	"Role": "home_page"
# }

# Website user home page (by function)
# get_website_user_home_page = "fire_base_admin.utils.get_home_page"

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Installation
# ------------

# before_install = "fire_base_admin.install.before_install"
# after_install = "fire_base_admin.install.after_install"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "fire_base_admin.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# Document Events
# ---------------
# Hook on document methods and events

doc_events = {
	"User": {
		"on_update": "fire_base_admin.auth_fire.create_user",
		"after_delete": "fire_base_admin.auth_fire.delete_user",
	}
}

# Scheduled Tasks
# ---------------

# scheduler_events = {
# 	"all": [
# 		"fire_base_admin.tasks.all"
# 	],
# 	"daily": [
# 		"fire_base_admin.tasks.daily"
# 	],
# 	"hourly": [
# 		"fire_base_admin.tasks.hourly"
# 	],
# 	"weekly": [
# 		"fire_base_admin.tasks.weekly"
# 	]
# 	"monthly": [
# 		"fire_base_admin.tasks.monthly"
# 	]
# }

# Testing
# -------

# before_tests = "fire_base_admin.install.before_tests"

# Overriding Methods
# ------------------------------
#
# override_whitelisted_methods = {
# 	"frappe.desk.doctype.event.event.get_events": "fire_base_admin.event.get_events"
# }
#
# each overriding function accepts a `data` argument;
# generated from the base implementation of the doctype dashboard,
# along with any modifications made in other Frappe apps
# override_doctype_dashboards = {
# 	"Task": "fire_base_admin.task.get_dashboard_data"
# }

# exempt linked doctypes from being automatically cancelled
#
# auto_cancel_exempted_doctypes = ["Auto Repeat"]

