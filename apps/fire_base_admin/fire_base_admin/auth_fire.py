from __future__ import unicode_literals
from firebase_admin import credentials, auth, initialize_app
import firebase_admin
import frappe
import os
import json 

if (not len(firebase_admin._apps)):
    __location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
    cred = credentials.Certificate(os.path.join(__location__, 'iwinch-firebase-adminsdk.json'))
    default_app = initialize_app(cred, {'storageBucket': 'iwinch.appspot.com'})


def validate_user(id_token):
    decoded_token = auth.verify_id_token(id_token)
    uid = decoded_token['uid']
    return uid

def create_user(user, method):
    print('---------------------------')
    print(user.is_new())
    if user.is_admin and frappe.form_dict.get('doc'):
        if user.is_new():
            user.new_password = user.new_password if user.new_password else 'welcome@iwinch'
            user_details = auth.create_user(email=user.email, password = user.new_password)
            print('Sucessfully created new user: {0}'.format(user_details.uid))
            user.uuid = user_details.uid
        else:
            data_dict = json.loads(frappe.form_dict['doc'])
            password = data_dict.get("new_password")
            data = {'email': user.email}
            if password:
                data['password'] = password
            print(data)
            auth.update_user(user.uuid, **data)
    return user


def delete_user(user, method):
    try:
        auth.delete_user(email=user.uuid)
    except:
        pass
    return user