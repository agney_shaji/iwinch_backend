from __future__ import unicode_literals
import frappe
# from frappe.utils.nestedset import NestedSet, get_ancestors_of, get_descendants_of


@frappe.whitelist()
def get_services(limit_start=0):
    doctype = 'Services'
    data = frappe.get_list(doctype, fields=[
        'name',
        'service_name',
        'image',
        'selected_image',
        'price_type',
        'priority',
        'service_desc'],
        order_by='service_name',
        limit_start=limit_start)
    return data


@frappe.whitelist()
def get_service_detail(service_name):
    # print(frappe.form_dict)
    doctype = 'Services'
    data = frappe.db.get_value(doctype, service_name, ['name', 'service_name', 'price_type', 'priority'], as_dict=1)
    return data


@frappe.whitelist()
def search_services(service_name='', limit_start=0):
    doctype = 'Services'
    filters = [['service_name', 'like', '{}%'.format(service_name)],]

    data = frappe.get_list(doctype, fields=[
        'name',
        'service_name',
        'image',
        'selected_image',
        'service_desc',
        'priority',
        'price_type'],
        filters=filters,
        order_by='name',
        limit_start=limit_start)
    return data