from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"label": _("Services"),
			"icon": "fa fa-star",
			"items": [
				{
					"type": "doctype",
					"name": "Services",
					"label": _("Services List"),
					"description": _("Services Provided"),
				},
			]
		},
	]
