# -*- coding: utf-8 -*-
# Copyright (c) 2020, agney and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
# import frappe
from frappe.model.document import Document

class RegionPricing(Document):


	def before_save(self):
		self.name = "{}-{}".format(self.parent, self.vh_type)
