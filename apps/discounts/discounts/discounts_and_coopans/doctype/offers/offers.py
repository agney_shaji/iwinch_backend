# -*- coding: utf-8 -*-
# Copyright (c) 2019, Agney Shaji and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
from frappe.utils import today
from fire_base_admin.rdb import download_file

class Offers(Document):

	def validate(self):
		self.flags.is_new_doc = self.is_new()
		to_day = today()
		
		if self.start_date < to_day and self.flags.is_new_doc:
			frappe.throw(_("You can not select past date in Start Date"))
		if self.start_date > self.end_date:
			frappe.throw(_("End Date should be grater than Start Date"))
		if self.start_date <= to_day <= self.end_date:
			self.is_active = 1
		else:
			self.is_active = 0


# def get_timeline_data(doctype, name):
# 	'''returns timeline data based on stock ledger entry'''
# 	out = {}
# 	doc = frappe.get_doc('Offers', name)
# 	items = dict(frappe.db.sql('''select posting_date, count(*)
# 		from `tabStock Ledger Entry` where item_code=%s
# 			and posting_date > date_sub(curdate(), interval 1 year)
# 			group by posting_date''', name))