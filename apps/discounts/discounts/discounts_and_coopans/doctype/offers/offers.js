// Copyright (c) 2019, Agney Shaji and contributors
// For license information, please see license.txt

frappe.ui.form.on('Offers', {
	setup: function(frm) {
		frm.set_query("region", function() {
			return {
				filters: {"is_group": 0}
			}
		});
		// frm.set_df_property("start_date", "read_only", frm.is_new() ? 0 : 1);
	},
	start_date: function(frm) {
		var today = frappe.datetime.get_today()
		if (frm.doc.start_date <= today && today <= frm.doc.end_date){

			frm.set_value("is_active", 1);
		}
		else{
			frm.set_value("is_active", 0);
		}
	},
	end_date: function(frm) {
		var today = frappe.datetime.get_today()
		if (frm.doc.start_date <= today && today <= frm.doc.end_date){

			frm.set_value("is_active", 1);
		}
		else{
			frm.set_value("is_active", 0);
		}
	},
	validate: function(frm) {
		if (frm.doc.start_date < frappe.datetime.get_today() && frm.doc.__islocal) {
			frappe.msgprint(__("You can not select past date in Start Date"));
			frappe.validated = false;
		}
		if (frm.doc.end_date < frm.doc.start_date) {
			frappe.msgprint(__("End Date should be grater than Start Date"));
			frappe.validated = false;
		}
	},
});

