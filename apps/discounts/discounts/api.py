from __future__ import unicode_literals
import frappe
from frappe.utils.nestedset import get_descendants_of


@frappe.whitelist()
def get_offers(offer_type="Offer"):
    doctype = 'Offers'
    filters = [['type', '=', '{}'.format(offer_type)], ['is_active', '=', 1]]
    # doctype_meta = frappe.get_meta(doctype)
    data = frappe.get_list(doctype, fields=[
        'name',
        'type',
        'title',
        're_link',
        'disc_img',
        'coupon_code'],
        filters=filters,
        order_by='name')
    return data


@frappe.whitelist()
def offer_detail_coupon(coupon_code):
    offer_name = frappe.db.sql("select name from `tabOffers` where coupon_code=%s",(coupon_code,))
    try:
        offer = frappe.get_doc('Offers', offer_name[0][0])
        return offer
    except expression as identifier:
        return None

@frappe.whitelist()
def offer_detail(offer_name):
    
    try:
        offer = frappe.get_doc('Offers', offer_name)
        return offer
    except expression as identifier:
        return None