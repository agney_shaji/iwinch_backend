from __future__ import unicode_literals
import frappe
from profiles.helper import buid_filters

@frappe.whitelist()
def list_offers(filters='{"name": ""}', start=0, order_by='creation desc', page_length=10):
    doctype = 'Offers'
    filter_dict = buid_filters(filters)
    data = frappe.get_list(doctype, fields=[
        'name',
        'title',
        'DATE_FORMAT(start_date, "%e-%m-%Y %H:%i") as start_date',
        'DATE_FORMAT(end_date, "%e-%m-%Y %H:%i") as end_date',
        'disc_per',
        'coupon_code',
        'DATE_FORMAT(creation, "%e-%m-%Y %H:%i") as creation',
        ],
        filters=filter_dict,
        order_by=order_by,
        page_length=page_length,
        start=start)
    count = frappe.get_all(doctype, fields=["COUNT(*) as count"], filters=filter_dict)[0]['count']
    return {'offers': data, 'count': count}