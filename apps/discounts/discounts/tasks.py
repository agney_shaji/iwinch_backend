from __future__ import unicode_literals
import frappe
import json
from frappe.utils import today, add_days

@frappe.whitelist()
def deactivate_offers():
    to_day = today()
    yesterday =  add_days(to_day, -1)

    de_offers = frappe.db.get_all('Offers',
                                  {'end_date': yesterday})
    for name in de_offers:
      frappe.db.set_value('Offers', name, 'is_active', 0)

    ac_offers = frappe.db.get_all('Offers',
                                  {'start_date': to_day})
    for name in ac_offers:
      frappe.db.set_value('Offers', name, 'is_active', 1)


def update_stats(order, method):
    if order.offer and order.status == 'Closed':
        offer = frappe.get_doc('Offers', order.offer)
        offer.dis_amt += order.discount_amount
        offer_user = frappe.get_value('Offer Usage', {'offer': order.offer, 'customer': order.customer}, ['name'])
        if offer_user:
            offer_profile = frappe.get_doc('Offer Usage', offer_user)
        else:
            offer_profile = frappe.get_doc({
                        'doctype': 'Offer Usage',
                        'offer': order.offer,
                        'customer': order.customer
                    })
            offer_profile.flags.ignore_permissions = True
            offer_profile.save()
            offer.appl_users += 1
        offer_profile.appl_count += 1
        offer.flags.ignore_permissions = True
        offer.save()
        offer_profile.flags.ignore_permissions = True
        offer_profile.save()