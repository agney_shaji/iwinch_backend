# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

with open('requirements.txt') as f:
	install_requires = f.read().strip().split('\n')

# get version from __version__ variable in discounts/__init__.py
from discounts import __version__ as version

setup(
	name='discounts',
	version=version,
	description='Discounts for services',
	author='Agney Shaji',
	author_email='agney@deienami.com',
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)
