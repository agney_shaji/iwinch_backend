from __future__ import unicode_literals
import frappe
from frappe.utils.background_jobs import enqueue
from notifications.push import push_data_notification

def create_notifications(notification):
    # enqueue(create_notification, 'default', event='all', notification=notification)
    enqueue(push_data_notification, 'default', event='all', notification=notification)
