from __future__ import unicode_literals
import frappe
from notifications.fcm import fcm_send_message, fcm_send_single_device_data_message


def push_notification(notification):
    registration_id = frappe.db.get_value('Devices', {'user': notification.recipient}, ['token'])
    result = fcm_send_message(
            registration_id=registration_id,
            title=notification.title,
            body=notification.message,
            data={'order_table':notification.order_table, 'order_name':notification.order_name, 'noti_type':notification.noti_type}
            )

def push_data_notification(notification):

    registration_id = frappe.db.get_value('Devices', {'user': notification.recipient}, ['token'])
    result = fcm_send_single_device_data_message(
            registration_id=registration_id,
            data_message={'order_table':notification.order_table, 'order_name':notification.order_name,
                          'noti_type':notification.noti_type, 'title': notification.title,
                          'message': notification.message}
            )

@frappe.whitelist()
def push_data_notification_test(**kwargs):

    registration_id = frappe.db.get_value('Devices', {'user': frappe.session.user}, ['token'])
    result = fcm_send_single_device_data_message(
            registration_id=registration_id,
            data_message={'order_table':"Sales Order", 'order_name':"olnnshshhs",
                          'noti_type':"order taken", 'title': "Pending",
                          'message': "New order waiting for conformation"}
            )

@frappe.whitelist()
def push_notification_test(**kwargs):
    registration_id = frappe.db.get_value('Devices', {'user': frappe.session.user}, ['token'])
    result = fcm_send_message(
            registration_id=registration_id,
            title="Pending",
            body="New order waiting for conformation",
            data={'order_table': "vdd", 'order_name':"ghhe", 'noti_type':"jjuiui"}
            )